@error($attributes->get('wire:model'))
    <input {{ $attributes->merge(["class" => "form-control is-invalid"]) }} accept="image/*" type="file" />
@else
    <input {{ $attributes->merge(["class" => "form-control"]) }} accept="image/*" type="file" />
@enderror

<div wire:loading wire:target="{{ $attributes->get('wire:model') }}">
    <div class="spinner-border spinner-border-sm text-primary" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
    <span class="text-primary">Uploading...</span>
</div>

@error($attributes->get('wire:model'))
  <small class="text-danger"> {{ $message }} </small>
@enderror
