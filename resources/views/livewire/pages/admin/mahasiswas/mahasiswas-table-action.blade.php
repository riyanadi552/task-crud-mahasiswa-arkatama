<div class="d-flex justify-content-center align-items-center gap-2">
    @can('admin-mahasiswa-update')
        <button wire:click="$dispatchTo('pages.admin.mahasiswas.mahasiswa-modal','edit', { id: '{{ $mahasiswas->id }}' })"
            class="btn btn-light btn-active-light-primary p-3 btn-center btn-sm">
            <i class="ki-outline ki-pencil fs-2"></i>
        </button>
    @endcan
    @can('admin-mahasiswa-delete')
        <button data-action-params='{{ json_encode(['id' => $mahasiswas->id]) }}' data-livewire-instance="@this"
            data-action="delete" data-action-receiver="pages.admin.mahasiswas.mahasiswa-modal" data-action-delete
            class="btn btn-light btn-active-light-primary p-3 btn-center btn-sm">
            <i class="ki-outline ki-trash fs-2"></i>
        </button>
    @endcan
</div>
