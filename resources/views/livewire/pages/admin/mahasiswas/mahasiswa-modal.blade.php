<div>
    <x-mollecules.modal id="add-mahasiswa_modal" action="store" wire:ignore.self>
        <x-slot:title>Add Mahasiswa</x-slot:title>
        <div class="">
            <div class="mb-6">
                <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
                <x-atoms.input name="nama_lengkap" wire:model='form.nama_lengkap' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>NIM</x-atoms.form-label>
                <x-atoms.input name="NIM" wire:model='form.NIM' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
                <x-atoms.radio-group>
                    <x-atoms.radio id="jenis_kelamin-L" name="jenis_kelamin" value="L"
                        wire:model='form.jenis_kelamin'>Laki-laki</x-atoms.radio>
                    <x-atoms.radio id="jenis_kelamin-P" name="jenis_kelamin" value="P"
                        wire:model='form.jenis_kelamin'>Perempuan</x-atoms.radio>
                </x-atoms.radio-group>
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
                <x-atoms.input name="tempat_lahir" wire:model='form.tempat_lahir' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
                <x-atoms.input name="tanggal_lahir" wire:model='form.tanggal_lahir' type="date" />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Email</x-atoms.form-label>
                <x-atoms.input name="email" wire:model='form.email' type="email" />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Nomor Telepon</x-atoms.form-label>
                <x-atoms.input name="nomor_telepon" wire:model='form.nomor_telepon' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Alamat Lengkap</x-atoms.form-label>
                <x-atoms.textarea name="alamat_lengkap" wire:model='form.alamat_lengkap' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Foto Profil</x-atoms.form-label>
                <x-atoms.img-upload name="foto_profil" wire:model="form.foto_profil" />
            </div>
            <x-slot:footer>
                <button class="btn-primary btn" type="submit">Submit</button>
            </x-slot:footer>
        </div>
    </x-mollecules.modal>
    <x-mollecules.modal id="edit-mahasiswa_modal" action="update" wire:ignore.self>
        <x-slot:title>Edit Mahasiswa</x-slot:title>
        <div class="">
            <div class="mb-6">
                <x-atoms.form-label required>Nama Lengkap</x-atoms.form-label>
                <x-atoms.input name="nama_lengkap" wire:model='form.nama_lengkap' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>NIM</x-atoms.form-label>
                <x-atoms.input name="NIM" wire:model='form.NIM' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Jenis Kelamin</x-atoms.form-label>
                <x-atoms.radio-group>
                    <x-atoms.radio id="jenis_kelamin-L" name="jenis_kelamin" value="L"
                        wire:model='form.jenis_kelamin'>Laki-laki</x-atoms.radio>
                    <x-atoms.radio id="jenis_kelamin-P" name="jenis_kelamin" value="P"
                        wire:model='form.jenis_kelamin'>Perempuan</x-atoms.radio>
                </x-atoms.radio-group>
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Tempat Lahir</x-atoms.form-label>
                <x-atoms.input name="tempat_lahir" wire:model='form.tempat_lahir' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Tanggal Lahir</x-atoms.form-label>
                <x-atoms.input name="tanggal_lahir" wire:model='form.tanggal_lahir' type="date" />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Email</x-atoms.form-label>
                <x-atoms.input name="email" wire:model='form.email' type="email" />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Nomor Telepon</x-atoms.form-label>
                <x-atoms.input name="nomor_telepon" wire:model='form.nomor_telepon' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label required>Alamat Lengkap</x-atoms.form-label>
                <x-atoms.textarea name="alamat_lengkap" wire:model='form.alamat_lengkap' />
            </div>
            <div class="mb-6">
                <x-atoms.form-label>Foto Profil</x-atoms.form-label>
                <x-atoms.img-upload name="foto_profil_baru" wire:model='form.foto_profil_baru' accept="image/*" />
                @if ($form->foto_profil == null)
                    <img src="{{ asset('assets/media/avatars/blank.png') }}" class="img-fluid mt-2" alt="preview" />
                @else
                    <img src="{{ asset('storage/uploads/' . $form->foto_profil) }}" class="img-fluid mt-2"
                        alt="preview" />
                @endif
            </div>
            <x-slot:footer>
                <button class="btn-primary btn" type="submit">Submit</button>
            </x-slot:footer>
        </div>
    </x-mollecules.modal>
</div>

@push('scripts')
    <script>
        document.addEventListener('livewire:initialized', () => {
            function refreshTable() {
                window.LaravelDataTables['mahasiswas-table'].ajax.reload();
            };
            @this.on('mahasiswa-added', () => {
                $('#add-mahasiswa_modal').modal('hide');
                Swal.fire({
                    title: "Success!",
                    text: "Berhasil Menambah Data Mahasiswa!",
                    icon: "success"
                });
                refreshTable();
            });
            @this.on('mahasiswa-deleted', () => {
                Swal.fire({
                    title: "Success!",
                    text: "Berhasil Menghapus Data Mahasiswa!",
                    icon: "success"
                });
                refreshTable();
            });
            @this.on('mahasiswa-edit', () => {
                $('#edit-mahasiswa_modal').modal('show');
                refreshTable();
            });
            @this.on('mahasiswa-updated', () => {
                $('#edit-mahasiswa_modal').modal('hide');
                Swal.fire({
                    title: "Success!",
                    text: "Berhasil Mengubah Data Mahasiswa!",
                    icon: "success"
                });
                refreshTable();
            });

            // $('#add-mahasiswa_modal, #edit-mahasiswa_modal').on('hidden.bs.modal', function(e) {
            //     $(this).find('form').trigger('reset');
            // });
        });
    </script>
@endpush
