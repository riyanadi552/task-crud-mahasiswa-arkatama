<x-layouts.app>
    <x-slot:title>Mahasiswas</x-slot:title>
    <livewire:pages.admin.mahasiswas.mahasiswa-modal />

    <div class="card">
        <div class="card-header border-0 pt-6">
            <div class="card-title">
                <div class="d-flex align-items-center position-relative my-1">
                    <span class="ki-outline ki-magnifier fs-3 position-absolute ms-5"></span>
                    <input type="text" data-table-id="mahasiswas-table"
                        class="form-control form-control-solid w-250px ps-13" placeholder="Search user"
                        id="mySearchInput" />
                </div>
                <div class="d-flex align-items-center position-relative my-1 mx-5">
                    <x-atoms.select class="form-select form-select-solid w-250px ps-13" data-action-filter
                        data-table-id="mahasiswas-table" data-filter="0">
                        <option value="">Pilih Filter</option>
                        <option value="3">NIM</option>
                        <option value="4">Nama Lengkap</option>
                        <option value="5">Email</option>
                        <option value="6">Jenis Kelamin</option>
                        <option value="7">Tempat Lahir</option>
                        <option value="8">Tanggal Lahir</option>
                        <option value="9">Nomor Telepon</option>
                        <option value="10">Alamat</option>
                    </x-atoms.select>
                </div>
            </div>

            <div class="card-toolbar">
                <!--begin::Toolbar-->
                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                    <!--begin::Add user-->
                    @can('admin-mahasiswa-create')
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#add-mahasiswa_modal">
                            <i class="ki-duotone ki-plus fs-2"></i>
                            <span>Add Mahasiswas</span>
                        </button>
                    @endcan
                </div>
            </div>
        </div>

        <div class="card-body py-4">
            <div class="table-responsive">
                {{ $dataTable->table() }}
            </div>
        </div>
    </div>

    @push('css')
        <link rel="stylesheet" href="{{ asset('assets/plugins/custom/datatables/datatables.bundle.css') }}">
    @endpush

    @push('scripts')
        <script src="{{ asset('assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
        {{ $dataTable->scripts() }}
        <script>
            $(document).ready(function() {
                var table = $('#mahasiswas-table').DataTable();

                $('#mySearchInput').on('keyup', function() {
                    var column = $('select[data-action-filter]').val();
                    var value = this.value;

                    if (column === "") {
                        table.search(value).draw();
                    } else {
                        table.column(column).search(value).draw();
                    }
                });

                $('select[data-action-filter]').on('change', function() {
                    table.search('').columns().search('').draw();
                    $('#mySearchInput').val('');
                });
            });
        </script>
    @endpush

</x-layouts.app>
