<?php

namespace App\Livewire\Forms;

use Livewire\Form;
use App\Models\MahasiswasModel;
use Livewire\Attributes\Rule;
use Livewire\Attributes\Validate;
use Illuminate\Validation\Rule as ValidationRule;
use Livewire\Features\SupportFileUploads\WithFileUploads;
use Illuminate\Support\Facades\Storage;

class MahasiswaForm extends Form
{
    #[Rule('required|min:3|max:255')]
    public $nama_lengkap = '';
    #[Rule('required|digits_between:10,11|unique:mahasiswas,NIM')]
    public $NIM = '';
    #[Rule('required|in:L,P')]
    public $jenis_kelamin = '';
    #[Rule('required|min:3|max:255')]
    public $tempat_lahir = '';
    #[Rule('required|date|before:today')]
    public $tanggal_lahir = '';
    #[Rule('required|email|unique:mahasiswas,email')]
    public $email = '';
    #[Rule('required|digits_between:10,13|unique:mahasiswas,nomor_telepon')]
    public $nomor_telepon = '';
    #[Rule('required')]
    public $alamat_lengkap = '';
    #[Rule('required|image|max:2048')]
    public $foto_profil;
    
    public $foto_profil_baru;
    
    public $id;

    public function save()
    {
        $validated = $this->validate();

        if ($this->foto_profil) {
            $filename = date('YmdHi') . '_' . str_replace(' ', '_', $this->foto_profil->getClientOriginalName());
            $this->foto_profil->storeAs('public/uploads/mahasiswas', $filename);
            $validated['foto_profil'] = 'mahasiswas/' . $filename;
        }
        MahasiswasModel::create($validated);
        $this->reset();
    }

    public function delete($id)
    {
        $mahasiswa = MahasiswasModel::find($id);
        
        if ($mahasiswa->foto_profil) {
            Storage::delete('public/uploads/' . $mahasiswa->foto_profil);
        }

        $mahasiswa->delete();
    }

    public function edit($id)
    {
        $mahasiswa = MahasiswasModel::find($id);
        $this->id = $id;
        $this->nama_lengkap = $mahasiswa->nama_lengkap;
        $this->NIM = $mahasiswa->NIM;
        $this->jenis_kelamin = $mahasiswa->jenis_kelamin;
        $this->tempat_lahir = $mahasiswa->tempat_lahir;
        $this->tanggal_lahir = $mahasiswa->tanggal_lahir;
        $this->email = $mahasiswa->email;
        $this->nomor_telepon = $mahasiswa->nomor_telepon;
        $this->alamat_lengkap = $mahasiswa->alamat_lengkap;
        $this->foto_profil = $mahasiswa->foto_profil;
    }

    public function update()
    {
        $validated = $this->validate([
            'nama_lengkap' => [
                'required',
                'min:3',
                'max:255',
            ],
            'NIM' => [
                'required',
                'digits_between:10,11',
                ValidationRule::unique('mahasiswas', 'NIM')->ignore((string) $this->id),
            ],
            'jenis_kelamin' => 'required|in:L,P',
            'tempat_lahir' => 'required|min:3|max:255',
            'tanggal_lahir' => 'required|date|before:today',
            'email' => [
                'required',
                'email',
                ValidationRule::unique('mahasiswas', 'email')->ignore((string) $this->id),
            ],
            'nomor_telepon' => [
                'required',
                'digits_between:10,13',
                ValidationRule::unique('mahasiswas', 'nomor_telepon')->ignore((string) $this->id),
            ],
            'alamat_lengkap' => 'required',
            'foto_profil_baru' => 'nullable|image|max:2048'
        ]);

        if($this->foto_profil_baru !== null){
            if($this->foto_profil !== ""){
                Storage::delete('public/uploads/' . $this->foto_profil);
            }
            $filename = date('YmdHi').'_'.str_replace(' ', '_', $this->foto_profil_baru->getClientOriginalName());
            $this->foto_profil_baru->storeAs('public/uploads/mahasiswas', $filename);
            $validated['foto_profil'] = 'mahasiswas/' . $filename;
        }

        MahasiswasModel::find((string) $this->id)->update($validated);
        $this->reset();
    }

}
