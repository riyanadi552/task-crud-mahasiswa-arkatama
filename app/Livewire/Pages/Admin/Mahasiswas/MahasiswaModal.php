<?php

namespace App\Livewire\Pages\Admin\Mahasiswas;

use Livewire\Component;
use App\Livewire\Forms\MahasiswaForm;
use App\Models\MahasiswasModel;
use Livewire\Attributes\On;
use Livewire\Attributes\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule as ValidationRule;
use Livewire\WithFileUploads;

class MahasiswaModal extends Component
{
    use WithFileUploads;

    public MahasiswaForm $form;

    public function store()
    {
        $this->form->save();
        $this->dispatch('mahasiswa-added');
    }

    #[On('delete')]
    public function delete($id)
    {
        $this->form->delete($id);
        $this->dispatch('mahasiswa-deleted');
    }

    #[On('edit')]
    public function edit($id)
    {
        $this->form->edit($id);    
        $this->dispatch("mahasiswa-edit");
    }

    public function update(){
        $this->form->update();
        $this->dispatch("mahasiswa-updated");
    }

    #[On('reset')]
    public function resetForm()
    {
        $this->form->reset();
        $this->form->resetValidation();
    }

    public function render()
    {
        return view('livewire.pages.admin.mahasiswas.mahasiswa-modal', [
            'parents' => MahasiswasModel::all(),
        ]);
    }
}
