<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswasModel extends Model
{
    use HasFactory;
    use HasUuids;
    protected $table = "mahasiswas";
    protected $fillable = [
        'nama_lengkap',
        'NIM',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'email',
        'nomor_telepon',
        'alamat_lengkap',
        'foto_profil'
    ];
}
