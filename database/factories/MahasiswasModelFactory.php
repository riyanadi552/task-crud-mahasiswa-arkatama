<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class MahasiswasModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'id' => $this->faker->uuid(),
            'nama_lengkap' => $this->faker->name(),
            'NIM' => $this->faker->unique()->numberBetween(1000000000, 9999999999),
            'jenis_kelamin' => $this->faker->randomElement(['L', 'P']),
            'tempat_lahir' => $this->faker->city(),
            'tanggal_lahir' => $this->faker->date(),
            'email' => $this->faker->unique()->safeEmail(),
            'nomor_telepon' => '08'.$this->faker->unique()->numerify('##########'),
            'alamat_lengkap' => $this->faker->address(),
        ];
    }
}
