<?php

namespace Database\Seeders;

use App\Models\MahasiswasModel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MahasiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        MahasiswasModel::factory()->count(10)->create();
    }
}
